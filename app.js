const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser')
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const fs = require('fs');
const { allowedNodeEnvironmentFlags } = require('process');
const { status } = require('express/lib/response');
const app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
app.use(logger('combined', { stream: accessLogStream }))

app.use(logger(':method :url :status :res[content-length] - :response-time ms'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

const filesFolder = path.join(__dirname, '/files')
try {
  if (!fs.existsSync(filesFolder)) {
    fs.mkdirSync(filesFolder)
  }
} catch (err) {
  throw err
}
// const allowedExtensions = filename.match(/.log$|.txt$|.json$|.yaml$|.xml$|.js$/i)

const createFile = function (req, res, next) {
  if (req.body.filename) {
    const testIfAllowed = req.body.filename.match(/.log$|.txt$|.json$|.yaml$|.xml$|.js$/i)
    console.log('test', testIfAllowed);
   
    // const extension  = req.body.filename.split('.')[1]
    // console.log('extention ', extension);
    if (!testIfAllowed) {
      res.status(400).json({ message: 'Wrong file extention' })
      throw new Error('Wrong file extention')
    }
    if (!req.body.content) {
      res.status(400).json({ message: 'File content is requred' })
    }
    const finallPath = `${path.join(__dirname, 'files/')}` + `${req.body.filename}`
    console.log(finallPath);
    if (!fs.existsSync(finallPath)) {
      fs.writeFile(finallPath, `${req.body.content}`, (err) => {
        if (err) throw err;
        console.log('File was created successfully');
      })
      res.status(200).json({message: 'success'})
    } else {
      res.status(400).json({ message: 'Error file already exists' });
      throw new Error('File already exists');
    }
  } else {
    res.status(400).json({ message: 'Body is empty' })
    throw new Error('Error body is empty');
  }
  next()
}

app.post('/api/files', createFile);


const getAllFiles = (req, res, next) => {
  const finallPath = path.join(__dirname, 'files')
  if (fs.existsSync(finallPath)) {
    fs.readdir(finallPath, (err, files) => {
      if (err) {
        console.log(err);
        res.status(400).json({ message: err })
      }
      res.status(200).json({ message: 'success', files })
      if (files.length > 0) {
        files.forEach(file => {
          console.log(file);
        });
      }
      next();
    });
  } else {
    res.status(500).json({ message: 'Wrong search directory' });
    throw new Error('Wrong search directory')
  }
}
app.get('/api/files', getAllFiles);

const getSpecificFile = (req, res, next) => {
  
  const filename = req.params.filename;
 
  const finallPath = path.join(__dirname, 'files', filename)

  if (fs.existsSync(finallPath)) { 
    const extension = req.params.filename.match(/.log$|.txt$|.json$|.yaml$|.xml$|.js$/i)[0].split('.')[1];
    fs.readFile(finallPath, 'utf8', (err, content) => {
      if (err) {
        console.error(err)
        return
      }
      res.status(200).json({ message: 'Success', filename, content, extension, uploadedDate: Date.now()})

      next();
    })
  } else {
    res.status(400).json({message: "File wasnt found"})
    throw new Error('File wasnt found')
  }
}
app.get('/api/files/:filename', getSpecificFile);
module.exports = app;
